import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import { Invoice, InvoiceNew } from './../src/invoice/invoice.entity';
import { InvoiceService } from './../src/invoice/invoice.service';
import {
  GetInvoiceResponse,
  InvoiceDTO,
  InvoiceNewDTO,
  PostInvoiceResponse,
} from 'src/invoice/invoice.dtos';

describe('AppController (e2e)', () => {
  let app: INestApplication;
  //let invoiceService: InvoiceService

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe({ transform: true }));
    await app.init();
  });

  describe('/ (GET)', () => {
    it('returns whatever', () => {
      return request(app.getHttpServer())
        .get('/')
        .expect(200)
        .expect('whatever');
    });
  });

  describe('/health (GET)', () => {
    it('returns OK', () => {
      return request(app.getHttpServer()).get('/health').expect(200).expect({
        status: 'OK',
        isConnectedToQueue: true,
        messagesSent: 0,
      });
    });
  });

  describe('/invoice (GET)', () => {
    let testInvoiceOne: Invoice;
    let testInvoiceTwo: Invoice;

    afterAll(async () => {
      const service = app.get<InvoiceService>(InvoiceService);
      await service.deleteInvoice(testInvoiceOne.id);
      await service.deleteInvoice(testInvoiceTwo.id);
    });

    it('allows retrieving all invoices', async () => {
      /* normally that would be a DB call most likely in before */
      const service = app.get<InvoiceService>(InvoiceService);
      const testInvoice: InvoiceNew = {
        customer: 'Nieznana Firma sp. z o.o.',
        supplier: 'Pewna Firma sp. z o.o.',
        issue_date: new Date('2023-04-13'),
        purchase_date: new Date('2023-04-14'),
        products: ['this', 'that'],
        net_price: 100,
        tax: 23,
        total_price: 123,
      };

      testInvoiceOne = await service.saveInvoice(testInvoice);
      testInvoiceTwo = await service.saveInvoice(testInvoice);

      const res = await request(app.getHttpServer()).get('/invoice');

      const expectedInvoice: InvoiceDTO = {
        id: 1,
        customer: 'Nieznana Firma sp. z o.o.',
        supplier: 'Pewna Firma sp. z o.o.',
        issue_date: '2023-04-13',
        purchase_date: '2023-04-14',
        products: ['this', 'that'],
        net_price: 100,
        tax: 23,
        total_price: 123,
      };
      expect(res.status).toEqual(200);
      const body = res.body as GetInvoiceResponse;
      expect(body.invoices).toHaveLength(2);
      expect(body.invoices).toContainEqual(expectedInvoice);
    });
  });

  describe('/invoice (POST)', () => {
    let newInvoice: InvoiceNewDTO;

    beforeEach(() => {
      newInvoice = {
        customer: 'Nieznana Firma sp. z o.o.',
        supplier: 'Pewna Firma sp. z o.o.',
        issue_date: '2023-04-14',
        purchase_date: '2023-04-13',
        products: ['this', 'that'],
        net_price: 100,
        tax: 23,
        total_price: 123,
      };
    });

    it('allows saving a valid invoice', async () => {
      console.log(newInvoice);
      const res = await request(app.getHttpServer())
        .post('/invoice')
        .send(newInvoice);

      const expectedInvoice: InvoiceDTO = {
        id: 1,
        customer: 'Nieznana Firma sp. z o.o.',
        supplier: 'Pewna Firma sp. z o.o.',
        issue_date: '2023-04-14',
        purchase_date: '2023-04-13',
        products: ['this', 'that'],
        net_price: 100,
        tax: 23,
        total_price: 123,
      };
      expect(res.status).toEqual(201);
      const body = res.body as PostInvoiceResponse;
      expect(body.invoice).toEqual(expectedInvoice);
    });

    it('rejects invalid date format', async () => {
      newInvoice.issue_date = 'tomorrow';

      const res = await request(app.getHttpServer())
        .post('/invoice')
        .send(newInvoice);
      expect(res.status).toEqual(400);
    });

    it('rejects missing supplier', async () => {
      newInvoice.supplier = undefined;

      const res = await request(app.getHttpServer())
        .post('/invoice')
        .send(newInvoice);
      expect(res.status).toEqual(400);
    });

    it('rejects tax and net not adding up to the total', async () => {
      newInvoice.total_price = 1;

      const res = await request(app.getHttpServer())
        .post('/invoice')
        .send(newInvoice);
      expect(res.status).toEqual(400);
    });

    it('rejects when issue before purchase', async () => {
      newInvoice.issue_date = '2021-01-01';

      const res = await request(app.getHttpServer())
        .post('/invoice')
        .send(newInvoice);
      expect(res.status).toEqual(400);
    });

    it('rejects when issue later than tonight midnight', async () => {
      const tomorrow = new Date();
      tomorrow.setDate(tomorrow.getDate() + 2);
      newInvoice.issue_date = tomorrow.toLocaleDateString('sv-SE');

      const res = await request(app.getHttpServer())
        .post('/invoice')
        .send(newInvoice);
      expect(res.status).toEqual(400);
    });
  });
});
