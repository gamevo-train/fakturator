import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';
import { Injectable } from '@nestjs/common';

@Injectable()
export class RabbitService {
  _messageCount: number;

  constructor(private readonly amqpConnection: AmqpConnection) {
    this._messageCount = 0;
  }

  async postMessage(message: any) {
    await this.amqpConnection.publish('event-stream', 'route1', message);
    this._messageCount++;
    return;
  }

  checkConnectionStatus() {
    return this.amqpConnection.connected;
  }

  getMessageSentCount() {
    return this._messageCount;
  }
}
