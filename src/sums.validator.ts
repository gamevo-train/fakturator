import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';

export function Sums(
  propertyOne: string,
  propertyTwo: string,
  validationOptions?: ValidationOptions,
) {
  return (object: any, propertyName: string) => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      options: validationOptions,
      constraints: [propertyOne, propertyTwo],
      validator: SumsConstraint,
    });
  };
}

@ValidatorConstraint({ name: 'Sums' })
export class SumsConstraint implements ValidatorConstraintInterface {
  validate(value: any, args: ValidationArguments) {
    const [propOne, propTwo] = args.constraints;
    const valueOne = (args.object as any)[propOne];
    const valueTwo = (args.object as any)[propTwo];
    return value === valueOne + valueTwo;
  }

  defaultMessage(args: ValidationArguments) {
    const [propOne, propTwo] = args.constraints;
    const valueOne = (args.object as any)[propOne];
    const valueTwo = (args.object as any)[propTwo];
    return `${propOne} = ${valueOne} and ${propTwo} = ${valueTwo} don't add up to ${args.property}`;
  }
}
