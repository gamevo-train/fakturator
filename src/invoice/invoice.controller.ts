import { Body, Controller, Get, Post } from '@nestjs/common';
import {
  GetInvoiceResponse,
  InvoiceNewDTO,
  PostInvoiceResponse,
} from './invoice.dtos';
import { dtoToInvoice, invoiceToDTO } from './invoice.helpers';
import { InvoiceService } from './invoice.service';

@Controller('invoice')
export class InvoiceController {
  constructor(private readonly service: InvoiceService) {}

  @Get()
  async getInvoices(): Promise<GetInvoiceResponse> {
    const invoices = await this.service.getInvoices();

    return {
      invoices: invoices.map(invoiceToDTO),
    };
  }

  @Post()
  async saveInvoice(@Body() dto: InvoiceNewDTO): Promise<PostInvoiceResponse> {
    const newInvoice = await this.service.saveInvoice(dtoToInvoice(dto));
    return {
      invoice: invoiceToDTO(newInvoice),
    };
  }
}
