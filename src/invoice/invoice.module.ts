import { Module } from '@nestjs/common';
import { RabbitModule } from './../rabbit/rabbit.module';
import { InvoiceController } from './invoice.controller';
import { InvoiceService } from './invoice.service';

@Module({
  imports: [RabbitModule],
  controllers: [InvoiceController],
  providers: [InvoiceService],
})
export class InvoiceModule {}
