import { InvoiceDTO, InvoiceNewDTO } from './invoice.dtos';
import { Invoice, InvoiceNew } from './invoice.entity';

export function dtoToInvoice(dto: InvoiceNewDTO): InvoiceNew {
  const newInvoice: InvoiceNew = {
    supplier: dto.supplier,
    customer: dto.customer,
    issue_date: new Date(dto.issue_date),
    purchase_date: new Date(dto.purchase_date),
    products: dto.products,
    net_price: dto.net_price,
    total_price: dto.total_price,
    tax: dto.tax,
  };

  return newInvoice;
}

export function invoiceToDTO(invoice: Invoice): InvoiceDTO {
  const dto = new InvoiceDTO();
  dto.id = invoice.id;
  dto.customer = invoice.customer;
  dto.supplier = invoice.supplier;
  dto.issue_date = invoice.issue_date.toLocaleDateString('sv-SE');
  dto.purchase_date = invoice.purchase_date.toLocaleDateString('sv-SE');
  dto.net_price = invoice.net_price;
  dto.tax = invoice.tax;
  dto.total_price = invoice.total_price;
  dto.products = invoice.products;
  return dto;
}
