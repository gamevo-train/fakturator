import { Injectable } from '@nestjs/common';
import { RabbitService } from '../rabbit/rabbit.service';
import { InvoiceMessage } from './invoice.dtos';
import { Invoice, InvoiceNew } from './invoice.entity';

@Injectable()
export class InvoiceService {
  private _invoices: Invoice[];

  constructor(private readonly queueService: RabbitService) {
    this._invoices = [];
  }

  async getInvoices(): Promise<Invoice[]> {
    return Promise.resolve(this._invoices);
  }

  saveInvoice(newInvoice: InvoiceNew): Promise<Invoice> {
    //TODO: generate id in the database
    const id = this._invoices.length + 1;
    const invoice = { ...newInvoice, id };
    this._invoices.push(invoice);
    this.notifyAboutInvoice({ action: 'insert', id: invoice.id });
    return Promise.resolve(invoice);
  }

  async getInvoice(id: number): Promise<Invoice | undefined> {
    const invoice = this._invoices.find((i) => i.id === id);
    return Promise.resolve(invoice);
  }

  deleteInvoice(id: number): Promise<void> {
    this._invoices = this._invoices.filter((i) => i.id !== id);
    this.notifyAboutInvoice({ action: 'delete', id });
    return;
  }
  /**
   * Publishes a message to whatever queue without waiting
   * @param message simplify event about a new invoice
   */
  notifyAboutInvoice(message: InvoiceMessage) {
    this.queueService
      .postMessage(message)
      .catch((e) =>
        console.error(
          `Failed to notify about ${message.action} of an invoice ${
            message.id
          }. Reason: ${e.message || 'unknown'}`,
        ),
      );
  }
}
