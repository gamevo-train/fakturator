import 'reflect-metadata';
import { InvoiceDTO, InvoiceNewDTO } from './invoice.dtos';
import { Invoice, InvoiceNew } from './invoice.entity';
import { dtoToInvoice, invoiceToDTO } from './invoice.helpers';

describe('InvoiceHelper', () => {
  describe('invoiceToDTO', () => {
    it('converts entity to DTO nicely', () => {
      const testInvoice = new Invoice();
      testInvoice.id = 1;
      testInvoice.customer = 'test customer';
      testInvoice.supplier = 'test supplier';
      testInvoice.issue_date = new Date('2023-02-02');
      testInvoice.purchase_date = new Date('2023-01-31');
      testInvoice.net_price = 100;
      testInvoice.tax = 23;
      testInvoice.total_price = 123;
      testInvoice.products = ['beer', 'deer', 'gear'];

      const expectedDto: InvoiceDTO = {
        id: 1,
        customer: 'test customer',
        supplier: 'test supplier',
        issue_date: '2023-02-02',
        purchase_date: '2023-01-31',
        net_price: 100,
        tax: 23,
        total_price: 123,
        products: ['beer', 'deer', 'gear'],
      };

      expect(invoiceToDTO(testInvoice)).toEqual(expectedDto);
    });
  });

  describe('dtoToInvoice', () => {
    it('converts dto to invoice well', () => {
      const dto: InvoiceNewDTO = {
        customer: 'test customer',
        supplier: 'test supplier',
        issue_date: '2023-02-02',
        purchase_date: '2023-01-31',
        net_price: 100,
        tax: 23,
        total_price: 123,
        products: ['beer', 'deer', 'gear'],
      };

      const expectedInvoice: InvoiceNew = {
        customer: 'test customer',
        supplier: 'test supplier',
        issue_date: new Date('2023-02-02'),
        purchase_date: new Date('2023-01-31'),
        net_price: 100,
        tax: 23,
        total_price: 123,
        products: ['beer', 'deer', 'gear'],
      };

      expect(dtoToInvoice(dto)).toEqual(expectedInvoice);
    });
  });
});
