import { Type } from 'class-transformer';
import { IsArray, IsDateString, IsNumber, IsString } from 'class-validator';
import { IsBefore } from '../isBefore.validator';
import { Sums } from '../sums.validator';

export class InvoiceNewDTO {
  @IsDateString({})
  @IsBefore()
  issue_date: string;

  @IsDateString({})
  @IsBefore('issue_date')
  purchase_date: string;

  @IsString()
  supplier: string;

  @IsString()
  customer: string;

  @IsArray()
  @Type(() => String)
  products: string[];

  @IsNumber()
  net_price: number;

  @IsNumber()
  tax: number;

  @IsNumber()
  @Sums('net_price', 'tax')
  total_price: number;
}

export class InvoiceDTO extends InvoiceNewDTO {
  id: number;
}

export class GetInvoiceResponse {
  invoices: InvoiceDTO[];
  /* some paging in future */
}

export class PostInvoiceResponse {
  invoice: InvoiceDTO;
}

export type MessageAction = 'insert' | 'delete';

export interface InvoiceMessage {
  action: MessageAction;
  id: number;
}
