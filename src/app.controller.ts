import { Controller, Get } from '@nestjs/common';
import { ApplicationHealthDTO } from './app.dtos';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getMain(): string {
    return 'whatever';
  }

  @Get('/health')
  getStatus(): ApplicationHealthDTO {
    return this.appService.getStatus();
  }
}
