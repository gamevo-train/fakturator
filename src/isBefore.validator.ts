import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';

export function IsBefore(
  dateProperty?: string,
  validationOptions?: ValidationOptions,
) {
  return (object: any, propertyName: string) => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      options: validationOptions,
      constraints: [dateProperty],
      validator: BeforeConstraint,
    });
  };
}

@ValidatorConstraint({ name: 'IsBefore' })
export class BeforeConstraint implements ValidatorConstraintInterface {
  validate(value: any, args: ValidationArguments) {
    const [dateProperty] = args.constraints;
    let comparedDate: Date;
    if (dateProperty) {
      const relatedDate = (args.object as any)[dateProperty];
      comparedDate = new Date(relatedDate);
    } else {
      comparedDate = new Date();
      comparedDate.setDate(comparedDate.getDate() + 1);
      comparedDate.setHours(0, 0, 0, 0);
    }
    const valueDate = new Date(value);
    return valueDate < comparedDate;
  }

  defaultMessage(args: ValidationArguments) {
    const [dateProperty] = args.constraints;
    return `${args.property} should be before ${
      dateProperty ?? 'tonight midnight'
    }`;
  }
}
