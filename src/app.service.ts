import { Injectable } from '@nestjs/common';
import { ApplicationHealthDTO } from './app.dtos';
import { RabbitService } from './rabbit/rabbit.service';

@Injectable()
export class AppService {
  constructor(private readonly queueService: RabbitService) {}

  getStatus(): ApplicationHealthDTO {
    return {
      status: 'OK',
      isConnectedToQueue: this.queueService.checkConnectionStatus(),
      messagesSent: this.queueService.getMessageSentCount(),
    };
  }
}
