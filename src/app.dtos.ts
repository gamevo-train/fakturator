export class ApplicationHealthDTO {
  status: string;
  isConnectedToQueue: boolean;
  messagesSent: number;
}
